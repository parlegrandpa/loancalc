/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { SafeAreaView, StyleSheet, ScrollView, StatusBar, View, Button, Alert } from 'react-native';
import {TextInput, Text} from 'react-native-paper';
import {
  Header,
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import {CustomLoader} from "./CustomLoader";


Number.prototype.format = function(n, x) {
    const re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
};

export default class App extends React.Component {

    state = {
        principal: 0,
        rate: 0,
        tenure: 0,
        loading: false,
        errorMessage: "",
        loanPayable: 0,
        monthlyLoanPayable: 0
    };

    onButtonClick()
    {
        const {principal, tenure, rate} = this.state;

        if(principal === 0 || tenure === 0 || rate === 0)
        {
            this.setState({loading: false, errorMessage: "All fields are required"});
        }
        else
        {
            this.setState({loading: true, errorMessage: ""});

            setTimeout( () => {
                this.setState({loading: false});

                this.calculateLoanInterest()
            }, 3000);
        }

    }

    calculateLoanInterest()
    {
        const principal = this.state.principal;
        const rate = this.state.rate;
        const tenure = this.state.tenure;

        const ratePerAnnum = rate / 100;

        const tenureInYears = tenure / 12;

        const loanPayable = principal * (1 + (ratePerAnnum * tenureInYears));

        const monthlyLoanPayable = loanPayable / tenure;

        console.log('loan', parseFloat(loanPayable).format(2))

        this.setState({loanPayable: parseFloat(loanPayable).format(2),
            monthlyLoanPayable: parseFloat(monthlyLoanPayable).format(2)});
    }

    render() {
        return (
            <>
                <StatusBar barStyle="dark-content"/>
                <SafeAreaView>
                    <ScrollView
                    contentInsetAdjustmentBehavior="automatic"
                    style={styles.scrollView}>

                    <Header/>
                    <View style={styles.viewStyle}>
                        <TextInput
                            mode='outlined'
                            label='Loan principal'
                            placeholder='Enter loan principal'
                            keyboardType='numeric'
                            style={styles.inputStyle}
                            value={this.state.principal}
                            onChangeText={(val) => this.setState({principal: val})}
                        />

                        <TextInput
                            mode='outlined'
                            label='Interest rate (%)'
                            placeholder='Enter interest rate'
                            keyboardType='numeric'
                            style={styles.inputStyle}
                            value={this.state.rate}
                            onChangeText={(val) => this.setState({rate: val})}
                        />

                        <TextInput
                            mode='outlined'
                            label='Loan tenure (months)'
                            placeholder='Enter loan tenure'
                            keyboardType='numeric'
                            style={styles.inputStyle}
                            value={this.state.tenure}
                            onChangeText={(val) => this.setState({tenure: val})}
                        />

                        <Text style={{color: 'red', fontStyle: 'italic'}}>{this.state.errorMessage}</Text>

                        <Button
                            title="Calculate"
                            onPress={() => this.onButtonClick()}
                        />


                        <View style={styles.loanResult}>
                            <View style={{flex: 0.5}}>
                                <Text>Amount Accrued</Text>
                            </View>
                            <View style={{flex: 0.5, marginStart: 50}}>
                                <Text>{'₦ ' + this.state.loanPayable}</Text>
                            </View>
                        </View>

                        <View style={styles.loanResult}>
                            <View style={{flex: 0.5}}>
                                <Text>Monthly Repayment</Text>
                            </View>
                            <View style={{flex: 0.5, marginStart: 50}}>
                                <Text>{'₦ ' + this.state.monthlyLoanPayable}</Text>
                            </View>
                        </View>

                        <CustomLoader visible={this.state.loading}/>
                    </View>

                    </ScrollView>
                </SafeAreaView>
            </>
        );
    };
};

const styles = StyleSheet.create({
    viewStyle: {
        padding: 20,
    },
    inputStyle: {
        marginBottom: 15,
        fontFamily: 'Lato',
    },
    loanResult: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 25,
        justifyContent: 'space-between',
    },
    scrollView: {
        backgroundColor: Colors.lighter,
    }
});
